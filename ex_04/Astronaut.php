<?php
namespace Astronaut;

include_once 'Mars.php';
class Astronaut {
    public static $lastId = 0; //oui
    public $id; // son id
    private $name; // son petit nom
    private $snacks = 0; // oui, pour le destructeur
    private $destination; // oui, sera utilisé pour le destructeur

    public function __construct($name, $snacks = 0, $destination = null)
    {
        //on parle ici de 4 attributs, on les sets, et rien de plus, pas besoin de faire de gros calculs, ou check dans le constructeur
        $this->name = $name;
        $this->id = self::$lastId++;
        $this->snacks = $snacks;
        $this->destination = $destination;
        echo $this->name . " ready for launch !" . PHP_EOL;
    }

    //partie getter et setter, rien de spéctaculaire, juste du code de base
    public static function getId() {
        return $this->id;
    }

    public function getDestination() {
        return $this->destination;
    }

    public function setDestination($destination){
        $this->destination = $destination;
    }

    public function getSnacks() {
        return $this->snacks;
    }

    public function setSnacks($snack){
        $this->snacks = $snack;
    }

    //la partie la plus importante, la vérification de chaque type de param passé
    public function doActions($param = null)
    {      
        if($param == null){
            echo $this->name . ": Nothing to do. \n";
        }
        //on vérifie ici que c'est bien une planete, grace au namespace qui différencie bien les deux, la fonction instanceof, va vérifier le type de l'objet
        if($param instanceof \planet\Mars){
            //action si planete
            $this->setDestination($param);
            echo $this->name . ": started a mission ! \n";
        }
        //pareil mais pour le chocolat
        if($param instanceof \chocolate\Mars){
            //on utilise le getter et le setter pour actualiser le nombre de snack totaux
            //le getter contient donc le nombre de snack mangé actuellement, (set a 0 depuis le constructeur ou l'attribut) puis on l'incrémente de 1
            $this->setSnacks($this->getSnacks() + 1);
            echo $this->name . ": is eating mars number" . $param->getId() . "\n";
        }
    }

    public function __destruct()
    {
        //si on a une destination, alors on le check, étant donné que dans le doaction du dessus on set la destination lorsqu'une planete est passé
        if($this->getDestination()){
                echo $this->name . ": Mission Aborted \n";
        }else {
            //sinon on affiche le nombre de snacks mangé, encore une fois grace aux infos défini dans le do actions
            echo $this->name . ": I may have done nothing, but I have " . $this->getSnacks() . " Mars to eat at least ! \n";
        }
        
    }
}


