<?php

/* première class Mars*/
 
namespace Chocolate;

class Mars
{
    public static $lastId = 0;
    public $id;

    public function __construct()
    {
        $this->id = self::$lastId++;
    }

    public function getId()
    {
        return $this->id;
    }
}


/* Deuxième class Planet*/
 
namespace Planet;

class Mars {
    
    public $size;
    public $name;
          
    public function __construct($size = null)
    {
        $this->size = $size;
        //echo " ma planet mesure " . $this->size.  PHP_EOL;
    }
    
    public static function getSize() {
        return $size;
    }

    public static function setSize($size) {
        $this->size = $size;
    }
}
//$rocks = new Mars(2);
//$lite = new Mars();
//$snack = new Mars("pertet");
//echo $rocks->getId() . "\n";
//echo $lite->getId() . "\n";
//echo $snack->getId() . "\n";S