<?php

/* Use static function as a counter */
  
class Astronaut {
      
    static $lastId = 0;
    private $id = 0;
    private $name;
    private $snacks = 0;
    private $destination;
      
    public function __construct($name, $snacks=0, $destination = null)
    {
        $this->id = self::$lastId++;
        $this->name = $name;
        $this->snacks = $snacks;
        $this->destination = $destination;

        echo $this->name . " ready for launch !" . PHP_EOL;
    }

    public function getId() {
        return $this->id;
    }

    public function getDestination() {
        return $destination;
    }

}