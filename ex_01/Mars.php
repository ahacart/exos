<?php
class Mars
{
    public static $lastId = 0;
    public $id;

    public function __construct()
    {
        $this->id = self::$lastId++;
    }

    public function getId()
    {
        return $this->id;
    }
}

